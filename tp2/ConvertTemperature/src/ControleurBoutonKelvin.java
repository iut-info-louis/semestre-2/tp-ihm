import javafx.event.EventHandler;
import javafx.event.ActionEvent;

public class ControleurBoutonKelvin implements EventHandler<ActionEvent> {
    private AppliConverter appli;
    private Temperature temperature;
    public ControleurBoutonKelvin(AppliConverter appli, Temperature temperature){
        this.appli = appli;
        this.temperature = temperature;
    }
    @Override
    public void handle(ActionEvent event){
        try {
            this.temperature.setValeurKelvin(this.appli.getValueKelvin());
            this.appli.majTF();
        } catch (NumberFormatException e) {
            this.appli.effaceTF();
        }
    }
}
