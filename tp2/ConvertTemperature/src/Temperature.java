public class Temperature {
    
    private double valeurCelcius;
    
    /**
     * Par défaut, la température est initialise à 0°C
     */
    public Temperature(){
        this.valeurCelcius = 0;
    }

    /**
     * @param  temperature  une tempéraure en degré Celcius (°C)
     */
    public Temperature(double temperature){
        this.valeurCelcius = temperature;
    }
    /**
     * 
     * @return double correspondant à la température en celsius
     */
    public double valeurCelcius(){
        return this.valeurCelcius;
    }

    /**
     * 
     * @return double correspondant à la température en Fahrenheit
     */
    public double valeurFahrenheit(){
        return this.valeurCelcius * 1.8 + 32;
    }

    /**
     * @param  nouvelleValeurCelcius  une tempéraure en degré Celcius (°C)
     */
    public void setvaleurCelcius(double nouvelleValeurCelcius){
        this.valeurCelcius = nouvelleValeurCelcius;
    }

    /**
     * @param  nouvelleValeurFahrenheit  une tempéraure en degré Fahrenheit (°F)
     */
    public void setvaleurFahrenheit(double nouvelleValeurFahrenheit){
        double nouvelleValeurCelcius = (nouvelleValeurFahrenheit - 32)/1.8;
        this.valeurCelcius = nouvelleValeurCelcius;
    }

    /**
     * 
     * @param nouvelleValeurKelvin une température en Kelvin (K)
     */
    public void setValeurKelvin(double nouvelleValeurKelvin){
        double nouvelleValeurCelcius = nouvelleValeurKelvin - 273.15;
        this.valeurCelcius = nouvelleValeurCelcius;
    }

    /**
     * 
     * @return double correspondant à la température en Kelvin
     */
    public double valeurKelvin(){
        return this.valeurCelcius + 273.15;
    }
    
    @Override
    public String toString(){
        return this.valeurCelcius+"°C = "+ this.valeurFahrenheit()+"°F";
    }
}

