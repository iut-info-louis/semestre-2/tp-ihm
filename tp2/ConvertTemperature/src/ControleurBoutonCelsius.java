import javafx.event.EventHandler;
import javafx.event.ActionEvent;

public class ControleurBoutonCelsius implements EventHandler<ActionEvent>{ 

    private AppliConverter appli;
    private Temperature temperature;
    
    public ControleurBoutonCelsius(AppliConverter appli, Temperature temperature){
        this.appli = appli;
        this.temperature = temperature;
    }

    @Override
    public void handle(ActionEvent event) {
        try{
            this.temperature.setvaleurCelcius(this.appli.getValueCelcius());
            this.appli.majTF();
        } catch (NumberFormatException e) {
            this.appli.effaceTF();
        }
    }
}
