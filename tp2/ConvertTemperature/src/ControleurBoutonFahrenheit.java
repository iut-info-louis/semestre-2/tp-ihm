import javafx.event.EventHandler;
import javafx.event.ActionEvent;

public class ControleurBoutonFahrenheit implements EventHandler<ActionEvent>{ 

    private AppliConverter appli;
    private Temperature temperature;
    
    public ControleurBoutonFahrenheit(AppliConverter appli, Temperature temperature){
        this.appli = appli;
        this.temperature = temperature;
    }

    @Override
    public void handle(ActionEvent event) {
        try {
            this.temperature.setvaleurFahrenheit(this.appli.getValueFahrenheit());
            this.appli.majTF();
        } catch (NumberFormatException e) {
            this.appli.effaceTF();
        }
    }
}
