package src2;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class AppliCalculatrice2 extends Application{
    private TextField textFieldN1;
    private TextField textFieldN2;
    private Label labelResultat;
    private Calcul calc;
    private RadioButton button1;
    private RadioButton button2;
    private RadioButton button3;
    private RadioButton button4;
    private Button boutonCalc;

    @Override
    public void init(){
        this.calc = new Calcul();
    }
    @Override
    public void start(Stage stage) throws Exception{
        // Construction du graphe de scène
        VBox root = new VBox();

        ajouteTF(root);
        ajouteBoutons(root);
        ajouteLResultat(root);

        Scene scene = new Scene(root);
        stage.setTitle("Calculatrice");
        stage.setScene(scene);
        stage.show();
    }

    public void effaceTF(){
        this.textFieldN1.setText("");
        this.textFieldN2.setText("");
        this.labelResultat.setText("Résultat : ");
    }
    public void majAffichage(){
        this.textFieldN1.setText(String.valueOf(this.calc.getNombre1()));
        this.textFieldN2.setText(String.valueOf(this.calc.getNombre2()));
        this.labelResultat.setText("Résultat : " + String.valueOf(this.calc.getResultat()));
    }

    public int getValuetextFieldN1() throws NumberFormatException{
        return Integer.parseInt(this.textFieldN1.getText());
    }
    public int getValuetextFieldN2() throws NumberFormatException{
        return Integer.parseInt(this.textFieldN2.getText());
    }

    public void quitte(){
        Platform.exit();
    }

    public void ajouteTF(Pane root){
        this.textFieldN1 = new TextField();
        this.textFieldN2 = new TextField();
        HBox hbTF = new HBox();
        hbTF.getChildren().addAll(textFieldN1, textFieldN2);
        root.getChildren().add(hbTF);
    }
        
    public void ajouteLResultat(Pane root){
        this.labelResultat = new Label("Résultat : ");
        HBox hbLabel = new HBox();
        hbLabel.getChildren().add(labelResultat);
        root.getChildren().add(hbLabel);
    }

    public void ajouteBoutons(Pane root){
        ToggleGroup group = new ToggleGroup();
        this.button1 = new RadioButton("+");
        this.button2 = new RadioButton("-");
        this.button3 = new RadioButton("*");
        this.button4 = new RadioButton("/");
        button1.setOnAction(new ControleurRadioButton(this, this.calc));
        button2.setOnAction(new ControleurRadioButton(this, this.calc));
        button3.setOnAction(new ControleurRadioButton(this, this.calc));
        button4.setOnAction(new ControleurRadioButton(this, this.calc));
        button1.setToggleGroup(group);
        button2.setToggleGroup(group);
        button3.setToggleGroup(group);
        button4.setToggleGroup(group);
        button1.setSelected(true);
        HBox hbBoutons = new HBox();
        hbBoutons.setSpacing(20);
        hbBoutons.getChildren().addAll(button1, button2, button3, button4);
        hbBoutons.setAlignment(Pos.CENTER);
        root.getChildren().add(hbBoutons);

        boutonCalc = new Button("Additionne");
        boutonCalc.setOnAction(new ControleurBoutonCalcul(this, this.calc));
        HBox hbBou = new HBox();
        hbBou.getChildren().add(boutonCalc);
        hbBou.setAlignment(Pos.CENTER);
        root.getChildren().add(hbBou);
    }

    public int getNum(){
        int i = 0;
        for (RadioButton et : Arrays.asList(button1, button2, button3, button4)){
            if(et.isSelected()){
                return i;
            }
            i++;
        }
        return 0;
    }
    public void majBoutonCalcul(String str){
        this.boutonCalc.setText(str);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
