package src2;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurBoutonCalcul implements EventHandler<ActionEvent> {
    private AppliCalculatrice2 app;
    private Calcul calc;
    
    public ControleurBoutonCalcul(AppliCalculatrice2 app, Calcul calcul){
        this.app = app;
        this.calc = calcul;
    }

    @Override
    public void handle(ActionEvent action){
        try {
            this.calc.setNombre1(this.app.getValuetextFieldN1());
            this.calc.setNombre2(this.app.getValuetextFieldN2());
            this.calc.calcul();
            this.calc.ajouterCalculHistorique();
            this.app.majAffichage();
        } catch (NumberFormatException e) {
            this.app.effaceTF();
        }
    }
}
