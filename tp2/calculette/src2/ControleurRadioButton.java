package src2;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurRadioButton implements EventHandler<ActionEvent> {
    private AppliCalculatrice2 app;
    private Calcul calc;
    
    public ControleurRadioButton(AppliCalculatrice2 app, Calcul calcul){
        this.app = app;
        this.calc = calcul;
    }
    @Override
    public void handle(ActionEvent action){
        int num = this.app.getNum();
        this.calc.setNumeroBouton(num);
        this.app.majBoutonCalcul(this.calc.getNomCalcul());
    }
}
