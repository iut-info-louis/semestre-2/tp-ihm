package src2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;

public class Calcul{
    private int nombre1;
    private int nombre2;
    private int res;
    private int numeroBouton;
    Map<Integer, String> dict = new HashMap<>();
    List<String> listCalculs;

    public Calcul(){
        this.nombre1 = 0;
        this.nombre2 = 0;
        this.res = 0;
        this.numeroBouton = 0;
        dict.put(3, "Divise");
        dict.put(2, "Multiplie");
        dict.put(1, "Soustrait");
        dict.put(0, "Additionne");
        this.listCalculs = new ArrayList<>();
    }
        //get set Nombre1
    public void setNombre1(int nombre){
        this.nombre1 = nombre;
    }
    public int getNombre1(){
        return this.nombre1;
    }
        //get set Nombre2
    public void setNombre2(int nombre){
        this.nombre2 = nombre;
    }
    public int getNombre2(){
        return this.nombre2;
    }
        // get set Resultat
    public void setResultat(int resultat){
        this.res = resultat;
    }
    public int getResultat(){
        return this.res;
    }
    public void setNumeroBouton(int numero){
        this.numeroBouton = numero;
    }
    public int getNumeroBouton(){
        return this.numeroBouton;
    }

    public String getNomCalcul(){
        return this.dict.get(this.numeroBouton);
    }
    public void calcul(){
        switch (this.numeroBouton) {
            case 3:
            this.res = this.nombre1 / this.nombre2;
                break;
            case 2:
            this.res = this.nombre1 * this.nombre2;
                break;
            case 1:
            this.res = this.nombre1 - this.nombre2;
                break;
            default:
            this.res = this.nombre1 + this.nombre2;
                break;
        }
    }

    public void ajouterCalculHistorique(){
        String str = String.valueOf(this.nombre1);
        switch (this.numeroBouton) {
            case 3:
            str += " / ";
                break;
            case 2:
            str += " * ";
                break;
            case 1:
            str += " - ";
                break;
            default:
            str += " + ";
                break;
        }
        str += String.valueOf(this.nombre2);
        this.listCalculs.add(str);
    }
}