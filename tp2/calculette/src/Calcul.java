package src;
public class Calcul{
    private int nombre1;
    private int nombre2;
    private int res;
    
    public Calcul(){
        this.nombre1 = 0;
        this.nombre2 = 0;
        this.res = 0;
    }
        //get set Nombre1
    public void setNombre1(int nombre){
        this.nombre1 = nombre;
    }
    public int getNombre1(){
        return this.nombre1;
    }
        //get set Nombre2
    public void setNombre2(int nombre){
        this.nombre2 = nombre;
    }
    public int getNombre2(){
        return this.nombre2;
    }
        // get set Resultat
    public void setResultat(int resultat){
        this.res = resultat;
    }
    public int getResultat(){
        return this.res;
    }
        //addition et soustraction
    public void additionne(){
        this.res = this.nombre1 + this.nombre2;
    }
    public void soustrait(){
        this.res = this.nombre1 - this.nombre2;
    }
}