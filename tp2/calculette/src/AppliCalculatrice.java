package src;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class AppliCalculatrice extends Application{
    protected TextField textFieldN1;
    protected TextField textFieldN2;
    protected Label labelResultat;
    private Calcul calc;
    
    @Override
    public void init(){
        this.calc = new Calcul();
    }
    @Override
    public void start(Stage stage) throws Exception{
        // Construction du graphe de scène
        VBox root = new VBox();

        ajouteTF1(root);
        ajouteTF2(root);
        ajouteBoutons(root);
        ajouteLResultat(root);

        Scene scene = new Scene(root);
        stage.setTitle("Calculatrice");
        stage.setScene(scene);
        stage.show();
    }

    public void effaceTF(){
        this.textFieldN1.setText("");
        this.textFieldN2.setText("");
        this.labelResultat.setText("Résultat : ");
    }
    public void majAffichage(){
        this.textFieldN1.setText(String.valueOf(this.calc.getNombre1()));
        this.textFieldN2.setText(String.valueOf(this.calc.getNombre2()));
        this.labelResultat.setText("Résultat : " + String.valueOf(this.calc.getResultat()));
    }

    public int getValuetextFieldN1() throws NumberFormatException{
        return Integer.parseInt(this.textFieldN1.getText());
    }
    public int getValuetextFieldN2() throws NumberFormatException{
        return Integer.parseInt(this.textFieldN2.getText());
    }

    public void quitte(){
        Platform.exit();
    }

    public void ajouteTF1(Pane root){
        HBox hbTF = new HBox();
        this.textFieldN1 = new TextField();
        hbTF.getChildren().add(textFieldN1);
        root.getChildren().add(hbTF);
    }
    public void ajouteTF2(Pane root){
        HBox hbTF = new HBox();
        this.textFieldN2 = new TextField();
        hbTF.getChildren().add(textFieldN2);
        root.getChildren().add(hbTF);
    }

    public void ajouteLResultat(Pane root){
        this.labelResultat = new Label("Résultat : ");
        HBox hbLabel = new HBox();
        hbLabel.getChildren().add(labelResultat);
        root.getChildren().add(hbLabel);
    }

    public void ajouteBoutons(Pane root){
        Button boutonAddition = new Button("+");
        Button boutonSoustraction = new Button("-");

        boutonAddition.setOnAction(new ControleurBoutonAddition(this, this.calc));
        boutonSoustraction.setOnAction(new ControleurBoutonSoustraction(this, this.calc));

        HBox hbBoutons = new HBox();
        hbBoutons.getChildren().addAll(boutonAddition, boutonSoustraction);
        hbBoutons.setAlignment(Pos.CENTER);
        root.getChildren().add(hbBoutons);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
