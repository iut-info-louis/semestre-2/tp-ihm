package src;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurBoutonAddition implements EventHandler<ActionEvent> {
    private AppliCalculatrice app;
    private Calcul calc;
    
    public ControleurBoutonAddition(AppliCalculatrice app, Calcul calcul){
        this.app = app;
        this.calc = calcul;
    }

    @Override
    public void handle(ActionEvent action){
        try {
            this.calc.setNombre1(this.app.getValuetextFieldN1());
            this.calc.setNombre2(this.app.getValuetextFieldN2());
            this.calc.additionne();
            this.app.majAffichage();
        } catch (NumberFormatException e) {
            this.app.effaceTF();
        }
    }
}
