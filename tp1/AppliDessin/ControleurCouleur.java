import javax.swing.text.AttributeSet.ColorAttribute;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.paint.Color;

public class ControleurCouleur implements EventHandler<ActionEvent> {
    private AppliDessin appli;
    private Color color ;
    
    public ControleurCouleur(AppliDessin appli, Color c){
        this.appli = appli;
        this.color = c;
    }
    public void handle(ActionEvent a){
        System.out.println("Couleur");
        this.appli.changeDerniereCouleur(color);
    }
}
