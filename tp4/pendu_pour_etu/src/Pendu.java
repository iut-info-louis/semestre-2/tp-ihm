import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.text.Text;

import java.util.List;
import java.util.Arrays;
import java.io.File;
import java.util.ArrayList;


/**
 * Vue du jeu du pendu
 */
public class Pendu extends Application {
    /**
     * modèle du jeu
     **/
    private MotMystere modelePendu;
    /**
     * Liste qui contient les images du jeu
     */
    private ArrayList<Image> lesImages;
    /**
     * Liste qui contient les noms des niveaux
     */    
    public List<String> niveaux;

    // les différents contrôles qui seront mis à jour ou consultés pour l'affichage
    /**
     * le dessin du pendu
     */
    private ImageView dessin;
    /**
     * le mot à trouver avec les lettres déjà trouvé
     */
    private Text motCrypte;
    /**
     * la barre de progression qui indique le nombre de tentatives
     */
    private ProgressBar pg;
    /**
     * le clavier qui sera géré par une classe à implémenter
     */
    private Clavier clavier;
    /**
     * le text qui indique le niveau de difficulté
     */
    private Text leNiveau;
    /**
     * le chronomètre qui sera géré par une clasee à implémenter
     */
    private Chronometre chrono;
    /**
     * le panel Central qui pourra être modifié selon le mode (accueil ou jeu)
     */
    private BorderPane panelCentral;
    /**
     * le bouton Paramètre / Engrenage
     */
    private Button boutonParametres;
    /**
     * le bouton Accueil / Maison
     */    
    private Button boutonMaison;
    /**
     * le bouton qui permet de (lancer ou relancer une partie
     */ 
    private Button bJouer;

    /**
     * le stage de l'affichage
     */
    private Stage stage;
    /**
     * initialise les attributs (créer le modèle, charge les images, crée le chrono ...)
     */
    @Override
    public void init(){
        this.modelePendu = new MotMystere("/usr/share/dict/french", 3, 10, MotMystere.FACILE, 10);
        this.clavier = new Clavier("AZERTYUIOPQSDFGHJKLMWXCVBN", new ControleurLettres(modelePendu, this));
        this.lesImages = new ArrayList<Image>();
        this.chargerImages("./img");

        this.niveaux = Arrays.asList("Facile", "Médium", "Difficile", "Expert");

        this.bJouer = new Button();
        this.bJouer.setOnAction(new ControleurLancerPartie(modelePendu, this));

        this.boutonMaison = new Button("", new ImageView(new Image("home.png", 50, 50, false, false)));
        this.boutonMaison.setOnAction(new RetourAccueil(modelePendu, this));
        this.boutonMaison.setMaxSize(100, 100);

        this.boutonParametres = new Button("", new ImageView(new Image("parametres.png", 50, 50, false, false)));
        this.boutonParametres.setOnAction(new ControleurInfos(this));
        this.boutonParametres.setMaxSize(100, 100);

        this.motCrypte = new Text();

        this.pg = new ProgressBar();
        this.pg.setMaxWidth(400);

        this.dessin = new ImageView();

        this.leNiveau = new Text();
    }

    /**
     * le graphe de scène de la vue à partir de methodes précédantes
     */
    private void laScene(){
        BorderPane fenetre = new BorderPane();
        fenetre.setTop(this.titre());
        fenetre.setCenter(this.panelCentral);
        this.stage.setScene(new Scene(fenetre, 800, 1000));
    }

    /**
     * @return le panel contenant le titre du jeu
     */
    private Pane titre(){
        BorderPane banniere = new BorderPane();
        banniere.setStyle("-fx-background-color: #7fd9e5");
        banniere.setPadding(new Insets(20));
        HBox hb = new HBox();
        hb.setSpacing(5);
        Text te = new Text("Jeu du Pendu");
        te.setFont(Font.font("Arial", 50));
        Button bInfo = new Button("", new ImageView(new Image("info.png", 50, 50, false, false)));
        bInfo.setOnAction(new ControleurInfos(this));
        banniere.setLeft(te);
        banniere.setRight(hb);
        hb.getChildren().addAll(boutonMaison, boutonParametres, bInfo);
        return banniere;
    }

    /**
     * @return le panel du chronomètre
     */
    private TitledPane leChrono(){
        // A implementer
        TitledPane res = new TitledPane();
        this.chrono = new Chronometre();
        res.setContent(this.chrono);
        res.setText("Chronomètre");
        res.setCollapsible(false);
        return res;
    }

    /**
     * @return la fenêtre de jeu avec le mot crypté, l'image, la barre
     *         de progression et le clavier
     */
    private BorderPane fenetreJeu(){
        // A implementer
        BorderPane res = new BorderPane();
        VBox vBuck = new VBox();
        VBox vBody = new VBox();
        res.setRight(vBuck);
        res.setCenter(vBody);

        vBuck.setPadding(new Insets(30));
        vBuck.setSpacing(30);
        VBox.setMargin(vBuck, new Insets(50));

        this.motCrypte.setText(modelePendu.getMotCrypte());
        this.dessin = new ImageView(this.lesImages.get(0));
        this.dessin.setFitHeight(550);
        this.dessin.setPreserveRatio(true);
        this.leNiveau.setText(niveaux.get(modelePendu.getNiveau()));
        TitledPane chrono = leChrono();
        this.bJouer.setText("Nouveau mot");
        // permet de laisser ne pas influencer la taille du clavier en fonction de la taille de l'écran
        Pane p = new Pane(this.clavier);
        this.clavier.setAlignment(Pos.CENTER);
        // vBody.setAlignment(Pos.CENTER);
        vBuck.getChildren().addAll(this.leNiveau, chrono, this.bJouer);

        vBody.getChildren().addAll(this.motCrypte, this.dessin, this.pg, p);

        return res;
    }

    /**
     * @return la fenêtre d'accueil sur laquelle on peut choisir les paramètres de jeu
     */
    private BorderPane fenetreAccueil(){
        //A implementer    
        BorderPane res = new BorderPane();
        res.setPadding(new Insets(10));
        TitledPane title = new TitledPane();
        title.setText("Niveau de difficulté");
        title.setCollapsible(false);
        this.bJouer.setText("Lancer une partie");
        ToggleGroup toggle = new ToggleGroup();
        VBox v = new VBox();
        v.setSpacing(10);
        v.getChildren().addAll(this.bJouer, title);
        res.setCenter(v);
        VBox v2 = new VBox();
        boolean t = true;
        ControleurNiveau con = new ControleurNiveau(modelePendu, this.niveaux);
        for(String elem : niveaux){
            RadioButton rad = new RadioButton(elem);
            rad.setToggleGroup(toggle);
            rad.setOnAction(con);
            if(t){t = false; rad.setSelected(true);}
            v2.getChildren().add(rad);
        }
        title.setContent(v2);
        return res;
    }

    /**
     * charge les images à afficher en fonction des erreurs
     * @param repertoire répertoire où se trouvent les images
     */
    // private void chargerImages(String repertoire){
    //     for (int i=0; i<this.modelePendu.getNbErreursMax()+1; i++){
    //         File file = new File(repertoire+"/pendu"+i+".png");
    //         System.out.println(file.toURI().toString());
    //         this.lesImages.add(new Image(file.toURI().toString()));
    //     }
    // }
        private void chargerImages(String repertoire){
            for (int i = 0; i < this.modelePendu.getNbErreursMax()+1; i++) {
                this.lesImages.add(new Image("pendu"+i+".png"));
            }
        }


    public void modeAccueil(){
        // A implementer
        this.panelCentral = fenetreAccueil();
        this.boutonMaison.setDisable(true);
        this.boutonParametres.setDisable(false);
        this.laScene();
    }
    
    public void modeJeu(){
        // A implementer
        this.boutonMaison.setDisable(false);
        this.boutonParametres.setDisable(true);
        this.panelCentral = fenetreJeu();
        this.laScene();
    }
    
    public void modeParametres(){
        // A implémenter
    }

    /** lance une partie */
    public void lancePartie(){
        // A implementer
        this.pg.setProgress(0);
        modelePendu.setMotATrouver();
        this.chrono.start();
        this.clavier = new Clavier("AZERTYUIOPQSDFGHJKLMWXCVBN", new ControleurLettres(modelePendu, this));
        modeJeu();
    }

    /**
     * raffraichit l'affichage selon les données du modèle
     */
    public void majAffichage(){
        // A implementer
        //pendant la partie
        this.pg.setProgress((10 - this.modelePendu.getNbErreursRestants())*0.1);
        this.motCrypte.setText(this.modelePendu.getMotCrypte());
        this.dessin.setImage(this.lesImages.get(10 - this.modelePendu.getNbErreursRestants()));
        this.clavier.desactiveTouches(this.modelePendu.getLettresEssayees());
    }

    /**
     * accesseur du chronomètre (pour les controleur du jeu)
     * @return le chronomètre du jeu
     */
    public Chronometre getChrono(){
        // A implémenter
        return this.chrono;
    }

    public Alert popUpPartieEnCours(){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION,"La partie est en cours!\n Etes-vous sûr de l'interrompre ?", ButtonType.YES, ButtonType.NO);
        alert.setTitle("Attention");
        return alert;
    }
        
    public Alert popUpReglesDuJeu(){
        // A implementer
        Alert alert = new Alert(Alert.AlertType.INFORMATION, "Le but du jeu est de découvrir le mot caché \nen donnant des lettres sans dépasser un certain \nnombre d'essai");
        alert.setTitle("Règles du jeu");
        return alert;
    }
    
    public Alert popUpMessageGagne(){
        // A implementer
        Alert alert = new Alert(Alert.AlertType.INFORMATION, "bravo, tu as gagné");
        alert.setTitle("Vous avez gagné :)");
        return alert;
    }
    
    public Alert popUpMessagePerdu(){
        // A implementer    
        Alert alert = new Alert(Alert.AlertType.INFORMATION, "tu as PERDU !!! le mot c'était "+ this.modelePendu.getMotATrouve());
        alert.setTitle("Vous avez perdu :|");
        return alert;
    }

    /**
     * créer le graphe de scène et lance le jeu
     * @param stage la fenêtre principale
     */
    @Override
    public void start(Stage stage) {
        this.stage = stage;
        stage.setTitle("IUTEAM'S - La plateforme de jeux de l'IUTO");
        this.modeAccueil();
        stage.show();
    }

    /**
     * Programme principal
     * @param args inutilisé
     */
    public static void main(String[] args) {
        launch(args);
    }    
}
