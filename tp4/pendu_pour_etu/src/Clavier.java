import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.layout.TilePane;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Génère la vue d'un clavier et associe le contrôleur aux touches
 * le choix ici est d'un faire un héritié d'un TilePane
 */
public class Clavier extends TilePane{
    /**
     * il est conseillé de stocker les touches dans un ArrayList
     */
    private List<Button> clavier;

    /**
     * constructeur du clavier
     * @param touches une chaine de caractères qui contient les lettres à mettre sur les touches
     * @param actionTouches le contrôleur des touches
     */
    public Clavier(String touches, EventHandler<ActionEvent> actionTouches) {
        // A implémenter
        this.setPrefColumns(10);
        this.setHgap(5);
        this.setVgap(5);
        clavier = new ArrayList<>();
        for (Character cha : touches.toCharArray()){
            Button temp = new Button(cha+"");
            temp.setOnAction(actionTouches);
            temp.setMinSize(30, 30);
            clavier.add(temp);
            this.getChildren().add(temp);
        }
        //fini
    }

    /**
     * permet de désactiver certaines touches du clavier (et active les autres)
     * @param touchesDesactivees une chaine de caractères contenant la liste des touches désactivées
     */
    public void desactiveTouches(Set<String> touchesDesactivees){
        // A implémenter
        for(Button b : clavier){
            if(touchesDesactivees.contains(b.getText())){
                b.setDisable(true);
            }
        }
        //fini
    }
}
