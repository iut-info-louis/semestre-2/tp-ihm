# Compilation

javac -d bin --module-path /usr/share/openjfx/lib/ --add-modules javafx.controls ./src/*.java

# Javadoc

javadoc -d doc --module-path /usr/share/openjfx/lib/ --add-modules javafx.controls ./src/*.java

# Execution

java -cp bin:img --module-path /usr/share/openjfx/lib/ --add-modules javafx.controls AppliConverter