import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class ApplicationChangement extends Application{
    private Button btn1;
    private Button btn2;
    private Scene scene;
    private Stage stage;
    public static void main(String[] args) {
        launch(ApplicationChangement.class, args);
    }
    @Override
    public void init(){
        this.btn1 = new Button("Connexion");        
        this.btn2 = new Button("Déconnexion");

        ControleurBouton controleur = new ControleurBouton(this);        
        this.btn1.setOnAction(controleur);
        this.btn2.setOnAction(controleur);
    }
    @Override
    public void start(Stage stage) {
        Pane root = new FenetreConnexion(this.btn1);
        this.scene = new Scene(root, 900, 700);
        stage.setScene(scene);
        stage.setTitle("Appli Analyste");
        this.stage = stage;
        this.stage.setHeight(240);
        this.stage.setWidth(300);
        stage.show();
    }

    public void afficheFenetreConnexion(){
        Pane root = new FenetreConnexion(this.btn1);
        this.scene.setRoot(root);
        this.stage.setHeight(240);
        this.stage.setWidth(300);
    }
    
    public void afficheFenetreAnalyste(){
        Pane root = new FenetreAnalyse(this.btn2);    
        this.scene.setRoot(root);
        this.stage.setHeight(600);
        this.stage.setWidth(1000);
    }
}