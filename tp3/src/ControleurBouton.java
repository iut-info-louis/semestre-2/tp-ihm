import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;

public class ControleurBouton implements EventHandler<ActionEvent>{

    ApplicationChangement app;
    public ControleurBouton(ApplicationChangement app){
        this.app = app;
    }
    public void handle(ActionEvent event){
        Button button = (Button) (event.getSource());
        if (button.getText().contains("Déconnexion"))
            this.app.afficheFenetreConnexion();
        else
            this.app.afficheFenetreAnalyste();
    }
}
