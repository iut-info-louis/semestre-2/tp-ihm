import javafx.geometry.Insets;
import javafx.geometry.Side;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class FenetreAnalyse extends BorderPane{

    private Button but;
    public FenetreAnalyse(Button but){
        super();
        this.but = but;
        
        BorderPane bPane = new BorderPane();
        initBorderPane(bPane);
        this.setCenter(bPane);
        }

    public void initBorderPane(BorderPane bPane){
        BorderPane top = new BorderPane();
        top.setPadding(new Insets(10));
        Text textTop = new Text("Allo 45 - Module Analyste");
        textTop.setFont(Font.font("Arial", FontWeight.BOLD , 32));
        Button boutonRight = but;
        boutonRight.setGraphic(new ImageView(new Image("user.png")));
        top.setStyle("-fx-background-color: #336699");
        top.setLeft(textTop);
        top.setRight(boutonRight);

        VBox center = new VBox();
        center.setPadding(new Insets(10));
        Text textVBox = new Text("Analyse du Sondage sur les habitudes alimentaires");
        textVBox.setFont(Font.font("Arial", 20));
        ComboBox<String> comboBox = new ComboBox<>();
        comboBox.getItems().addAll("pie", "line", "bar", "data", "location", "area", "relative");
        comboBox.getSelectionModel().select(0);
        PieChart pieChart = new PieChart();
        pieChart.setTitle("Que lisez-vous au petit déjeuner ?");
        pieChart.getData().setAll(
            new PieChart.Data("Le journal", 21),
            new PieChart.Data("Un livre", 3),
            new PieChart.Data("Le courier", 7),
            new PieChart.Data("La boîte de céréales", 75));
            pieChart.setLegendSide(Side.LEFT);
        HBox hBox = new HBox();
        Button b1 = new Button("Question précédente", new ImageView(new Image("back.png")));
        Button b2 = new Button("Question suivante", new ImageView(new Image("next.png")));
        hBox.getChildren().addAll(b1, b2);
        hBox.setSpacing(10);
        center.getChildren().addAll(textVBox, comboBox, pieChart, hBox);

        TilePane right = new TilePane();
        right.setVgap(5);
        right.setHgap(5);
        right.setStyle("-fx-background-color: #DAE6F3");
        ImageView image1 = new ImageView(new Image("chart_1.png"));
        ImageView image2 = new ImageView(new Image("chart_2.png"));
        ImageView image3 = new ImageView(new Image("chart_3.png"));
        ImageView image4 = new ImageView(new Image("chart_4.png"));
        ImageView image5 = new ImageView(new Image("chart_5.png"));
        ImageView image6 = new ImageView(new Image("chart_6.png"));
        ImageView image7 = new ImageView(new Image("chart_7.png"));
        right.getChildren().addAll(image1, image2, image3, image4, image5, image6, image7);

        bPane.setTop(top);
        bPane.setCenter(center);
        bPane.setRight(right);
    }
}