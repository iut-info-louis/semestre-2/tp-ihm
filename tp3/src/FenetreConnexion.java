import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;

public class FenetreConnexion extends BorderPane {

    private Button but;
    public FenetreConnexion(Button but){
        super();
        this.but = but;
    
        GridPane grid = new GridPane();
        initGrid(grid);
        this.setCenter(grid);
        }

    private void initGrid(GridPane grid){
        Label labelUsername = new Label("Identifiant");
        Label labelPassword = new Label("Mot de passe");
        TextField tfUsername = new TextField();
        TextField tfPassword = new TextField();
        Button boutonConnexion = but;
        grid.add(labelUsername, 0, 1);
        grid.add(labelPassword, 0, 2);
        grid.add(tfUsername, 1, 1);
        grid.add(tfPassword, 1, 2);
        grid.add(boutonConnexion, 1, 3);
        GridPane.setHalignment(boutonConnexion, HPos.RIGHT);
        grid.setVgap(12);
        grid.setHgap(10);
        grid.setPadding(new Insets(50));
    }
}